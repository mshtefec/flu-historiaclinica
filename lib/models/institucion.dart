import 'dart:convert';

Institucion institucionFromJson(String str) => Institucion.fromJson(json.decode(str));

String institucionToJson(Institucion data) => json.encode(data.toJson());

class Institucion {
    Institucion({
        this.id,
        this.historiaId,
        this.calificacion,
        this.evolucion,
        this.recepcion,
    });

    int id;
    int historiaId;
    String calificacion;
    String evolucion;
    String recepcion;

    factory Institucion.fromJson(Map<String, dynamic> json) => Institucion(
        id: json["id"],
        historiaId: json["historiaId"],
        calificacion: json["calificacion"],
        evolucion: json["evolucion"],
        recepcion: json["recepcion"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "historiaId": historiaId,
        "calificacion": calificacion,
        "evolucion": evolucion,
        "recepcion": recepcion,
    };
}
