
import 'dart:convert';

Ginecobstetrico ginecobstetricoFromJson(String str) => Ginecobstetrico.fromJson(json.decode(str));

String ginecobstetricoToJson(Ginecobstetrico data) => json.encode(data.toJson());

class Ginecobstetrico {
    Ginecobstetrico({
        this.id,
        this.historiaId,
        this.estadoId,
        this.aborto,
        this.embarazo,
        this.hemorragiaGenital,
        this.otros,
        this.parto,
        this.tactoVaginal,
    });

    int id;
    int historiaId;
    int estadoId;
    String aborto;
    String embarazo;
    bool hemorragiaGenital;
    String otros;
    String parto;
    String tactoVaginal;

    factory Ginecobstetrico.fromJson(Map<String, dynamic> json) => Ginecobstetrico(
        id: json["id"],
        historiaId: json["historiaId"],
        estadoId: json["estadoId"],
        aborto: json["aborto"],
        embarazo: json["embarazo"],
        hemorragiaGenital: json["hemorragiaGenital"],
        otros: json["otros"],
        parto: json["parto"],
        tactoVaginal: json["tactoVaginal"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "historiaId": historiaId,
        "estadoId": estadoId,
        "aborto": aborto,
        "embarazo": embarazo,
        "hemorragiaGenital": hemorragiaGenital,
        "otros": otros,
        "parto": parto,
        "tactoVaginal": tactoVaginal,
    };
}
