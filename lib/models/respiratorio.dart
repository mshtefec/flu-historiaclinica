import 'dart:convert';

Respiratorio respiratorioFromJson(String str) => Respiratorio.fromJson(json.decode(str));

String respiratorioToJson(Respiratorio data) => json.encode(data.toJson());

class Respiratorio {
    Respiratorio({
        this.id,
        this.historiaId,
        this.estadoId,
        this.apnea,
        this.asistido,
        this.broncoaspiracion,
        this.cuerpoExtrano,
        this.estridor,
        this.frotePleural,
        this.hemoptisis,
        this.irregular,
        this.matidezPercutoriaColumna,
        this.matidezPercutoriaCamposPulmonares,
        this.musculosAccesorios,
        this.murmulloVesicularAusente,
        this.otros,
        this.paradojal,
        this.rale,
        this.roncus,
        this.soploTubario,
        this.substancias,
    });

    int id;
    int historiaId;
    int estadoId;
    bool apnea;
    bool asistido;
    bool broncoaspiracion;
    bool cuerpoExtrano;
    bool estridor;
    String frotePleural;
    bool hemoptisis;
    bool irregular;
    bool matidezPercutoriaColumna;
    String matidezPercutoriaCamposPulmonares;
    bool musculosAccesorios;
    String murmulloVesicularAusente;
    String otros;
    bool paradojal;
    String rale;
    bool roncus;
    String soploTubario;
    bool substancias;

    factory Respiratorio.fromJson(Map<String, dynamic> json) => Respiratorio(
        id: json["id"],
        historiaId: json["historiaId"],
        estadoId: json["estadoId"],
        apnea: json["apnea"],
        asistido: json["asistido"],
        broncoaspiracion: json["broncoaspiracion"],
        cuerpoExtrano: json["cuerpoExtrano"],
        estridor: json["estridor"],
        frotePleural: json["frotePleural"],
        hemoptisis: json["hemoptisis"],
        irregular: json["irregular"],
        matidezPercutoriaColumna: json["matidezPercutoriaColumna"],
        matidezPercutoriaCamposPulmonares: json["matidezPercutoriaCamposPulmonares"],
        musculosAccesorios: json["musculosAccesorios"],
        murmulloVesicularAusente: json["murmulloVesicularAusente"],
        otros: json["otros"],
        paradojal: json["paradojal"],
        rale: json["rale"],
        roncus: json["roncus"],
        soploTubario: json["soploTubario"],
        substancias: json["substancias"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "historiaId": historiaId,
        "estadoId": estadoId,
        "apnea": apnea,
        "asistido": asistido,
        "broncoaspiracion": broncoaspiracion,
        "cuerpoExtrano": cuerpoExtrano,
        "estridor": estridor,
        "frotePleural": frotePleural,
        "hemoptisis": hemoptisis,
        "irregular": irregular,
        "matidezPercutoriaColumna": matidezPercutoriaColumna,
        "matidezPercutoriaCamposPulmonares": matidezPercutoriaCamposPulmonares,
        "musculosAccesorios": musculosAccesorios,
        "murmulloVesicularAusente": murmulloVesicularAusente,
        "otros": otros,
        "paradojal": paradojal,
        "rale": rale,
        "roncus": roncus,
        "soploTubario": soploTubario,
        "substancias": substancias,
    };
}
