import 'dart:convert';

Direccion direccionFromJson(String str) => Direccion.fromJson(json.decode(str));

String direccionToJson(Direccion data) => json.encode(data.toJson());

class Direccion {
    Direccion({
        this.id,
        this.historiaId,
        this.direccionAtencionId,
        this.latitud,
        this.longitud,
        this.calle,
        this.numero,
        this.piso,
        this.depto,
        this.casa,
        this.monoblock,
        this.barrio,
        this.entreCalle,
        this.yCalle,
        this.localidad,
    });

    int id;
    int historiaId;
    int direccionAtencionId;
    String latitud;
    String longitud;
    String calle;
    String numero;
    String piso;
    String depto;
    String casa;
    String monoblock;
    String barrio;
    String entreCalle;
    String yCalle;
    String localidad;

    factory Direccion.fromJson(Map<String, dynamic> json) => Direccion(
        id: json["id"],
        historiaId: json["historiaId"],
        direccionAtencionId: json["direccionAtencionId"],
        latitud: json["latitud"],
        longitud: json["longitud"],
        calle: json["calle"],
        numero: json["numero"],
        piso: json["piso"],
        depto: json["depto"],
        casa: json["casa"],
        monoblock: json["monoblock"],
        barrio: json["barrio"],
        entreCalle: json["entreCalle"],
        yCalle: json["yCalle"],
        localidad: json["localidad"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "historiaId": historiaId,
        "direccionAtencionId": direccionAtencionId,
        "latitud": latitud,
        "longitud": longitud,
        "calle": calle,
        "numero": numero,
        "piso": piso,
        "depto": depto,
        "casa": casa,
        "monoblock": monoblock,
        "barrio": barrio,
        "entreCalle": entreCalle,
        "yCalle": yCalle,
        "localidad": localidad,
    };
}
