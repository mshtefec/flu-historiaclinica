import 'dart:convert';

Trauma traumaFromJson(String str) => Trauma.fromJson(json.decode(str));

String traumaToJson(Trauma data) => json.encode(data.toJson());

class Trauma {
    Trauma({
        this.id,
        this.historiaId,
        this.tipoLesionId,
        this.zonaId,
        this.mecanismoId,
    });

    int id;
    int historiaId;
    int tipoLesionId;
    int zonaId;
    int mecanismoId;

    factory Trauma.fromJson(Map<String, dynamic> json) => Trauma(
        id: json["id"],
        historiaId: json["historiaId"],
        tipoLesionId: json["tipoLesionId"],
        zonaId: json["zonaId"],
        mecanismoId: json["mecanismoId"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "historiaId": historiaId,
        "tipoLesionId": tipoLesionId,
        "zonaId": zonaId,
        "mecanismoId": mecanismoId,
    };
}
