import 'dart:convert';

Cardiovascular cardiovascularFromJson(String str) => Cardiovascular.fromJson(json.decode(str));

String cardiovascularToJson(Cardiovascular data) => json.encode(data.toJson());

class Cardiovascular {
    Cardiovascular({
        this.id,
        this.historiaId,
        this.estadoId,
        this.arritmia,
        this.desdoblados,
        this.electrocardiograma,
        this.frotePericardico,
        this.galope,
        this.ingurYugular,
        this.otros,
        this.pulsosPerifericos,
        this.r3,
        this.r4,
        this.ruidosHipofoneticos,
        this.soploSistolico,
        this.soploDiastolico,
    });

    int id;
    int historiaId;
    int estadoId;
    String arritmia;
    String desdoblados;
    String electrocardiograma;
    bool frotePericardico;
    bool galope;
    bool ingurYugular;
    String otros;
    String pulsosPerifericos;
    bool r3;
    bool r4;
    String ruidosHipofoneticos;
    String soploSistolico;
    String soploDiastolico;

    factory Cardiovascular.fromJson(Map<String, dynamic> json) => Cardiovascular(
        id: json["id"],
        historiaId: json["historiaId"],
        estadoId: json["estadoId"],
        arritmia: json["arritmia"],
        desdoblados: json["desdoblados"],
        electrocardiograma: json["electrocardiograma"],
        frotePericardico: json["frotePericardico"],
        galope: json["galope"],
        ingurYugular: json["ingurYugular"],
        otros: json["otros"],
        pulsosPerifericos: json["pulsosPerifericos"],
        r3: json["r3"],
        r4: json["r4"],
        ruidosHipofoneticos: json["ruidosHipofoneticos"],
        soploSistolico: json["soploSistolico"],
        soploDiastolico: json["soploDiastolico"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "historiaId": historiaId,
        "estadoId": estadoId,
        "arritmia": arritmia,
        "desdoblados": desdoblados,
        "electrocardiograma": electrocardiograma,
        "frotePericardico": frotePericardico,
        "galope": galope,
        "ingurYugular": ingurYugular,
        "otros": otros,
        "pulsosPerifericos": pulsosPerifericos,
        "r3": r3,
        "r4": r4,
        "ruidosHipofoneticos": ruidosHipofoneticos,
        "soploSistolico": soploSistolico,
        "soploDiastolico": soploDiastolico,
    };
}
