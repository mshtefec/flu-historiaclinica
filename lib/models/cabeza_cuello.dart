import 'dart:convert';

CabezaCuello cabezaCuelloFromJson(String str) => CabezaCuello.fromJson(json.decode(str));

String cabezaCuelloToJson(CabezaCuello data) => json.encode(data.toJson());

class CabezaCuello {
    CabezaCuello({
        this.id,
        this.historiaId,
        this.estadoId,
        this.descripcion,
    });

    int id;
    int historiaId;
    int estadoId;
    String descripcion;

    factory CabezaCuello.fromJson(Map<String, dynamic> json) => CabezaCuello(
        id: json["id"],
        historiaId: json["historiaId"],
        estadoId: json["estadoId"],
        descripcion: json["descripcion"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "historiaId": historiaId,
        "estadoId": estadoId,
        "descripcion": descripcion,
    };
}
