import 'dart:convert';

DatosAfiliado datosAfiliadoFromJson(String str) => DatosAfiliado.fromJson(json.decode(str));

String datosAfiliadoToJson(DatosAfiliado data) => json.encode(data.toJson());

class DatosAfiliado {
    DatosAfiliado({
        this.id,
        this.historiaId,
        this.nombre,
        this.edad,
        this.sexo,
        this.nroDocumento,
    });

    int id;
    int historiaId;
    String nombre;
    int edad;
    String sexo;
    String nroDocumento;

    factory DatosAfiliado.fromJson(Map<String, dynamic> json) => DatosAfiliado(
        id: json["id"],
        historiaId: json["historiaId"],
        nombre: json["nombre"],
        edad: json["edad"],
        sexo: json["sexo"],
        nroDocumento: json["nroDocumento"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "historiaId": historiaId,
        "nombre": nombre,
        "edad": edad,
        "sexo": sexo,
        "nroDocumento": nroDocumento,
    };
}
