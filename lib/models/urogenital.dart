import 'dart:convert';

Urogenital urogenitalFromJson(String str) => Urogenital.fromJson(json.decode(str));

String urogenitalToJson(Urogenital data) => json.encode(data.toJson());

class Urogenital {
    Urogenital({
        this.id,
        this.historiaId,
        this.estadoId,
        this.anuria,
        this.disuria,
        this.dolorPpr,
        this.dolorPru,
        this.dolorPv,
        this.estranguria,
        this.genitales,
        this.globoVesical,
        this.hematuria,
        this.oliguria,
        this.otros,
        this.poliuria,
        this.sondaVesical,
    });

    int id;
    int historiaId;
    int estadoId;
    bool anuria;
    bool disuria;
    String dolorPpr;
    String dolorPru;
    String dolorPv;
    bool estranguria;
    String genitales;
    bool globoVesical;
    bool hematuria;
    bool oliguria;
    String otros;
    bool poliuria;
    bool sondaVesical;

    factory Urogenital.fromJson(Map<String, dynamic> json) => Urogenital(
        id: json["id"],
        historiaId: json["historiaId"],
        estadoId: json["estadoId"],
        anuria: json["anuria"],
        disuria: json["disuria"],
        dolorPpr: json["dolorPpr"],
        dolorPru: json["dolorPru"],
        dolorPv: json["dolorPv"],
        estranguria: json["estranguria"],
        genitales: json["genitales"],
        globoVesical: json["globoVesical"],
        hematuria: json["hematuria"],
        oliguria: json["oliguria"],
        otros: json["otros"],
        poliuria: json["poliuria"],
        sondaVesical: json["sondaVesical"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "historiaId": historiaId,
        "estadoId": estadoId,
        "anuria": anuria,
        "disuria": disuria,
        "dolorPpr": dolorPpr,
        "dolorPru": dolorPru,
        "dolorPv": dolorPv,
        "estranguria": estranguria,
        "genitales": genitales,
        "globoVesical": globoVesical,
        "hematuria": hematuria,
        "oliguria": oliguria,
        "otros": otros,
        "poliuria": poliuria,
        "sondaVesical": sondaVesical,
    };
}
