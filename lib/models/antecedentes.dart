import 'dart:convert';

Antecedentes antecedentesFromJson(String str) => Antecedentes.fromJson(json.decode(str));

String antecedentesToJson(Antecedentes data) => json.encode(data.toJson());

class Antecedentes {
    Antecedentes({
        this.id,
        this.historiaId,
        this.estadoId,
        this.diabetesI,
        this.diabetesIi,
        this.asma,
        this.cardiopatia,
        this.covulsion,
        this.hta,
        this.epoc,
        this.acv,
    });

    int id;
    int historiaId;
    int estadoId;
    bool diabetesI;
    bool diabetesIi;
    bool asma;
    bool cardiopatia;
    bool covulsion;
    bool hta;
    bool epoc;
    bool acv;

    factory Antecedentes.fromJson(Map<String, dynamic> json) => Antecedentes(
        id: json["id"],
        historiaId: json["historiaId"],
        estadoId: json["estadoId"],
        diabetesI: json["diabetesI"],
        diabetesIi: json["diabetesIi"],
        asma: json["asma"],
        cardiopatia: json["cardiopatia"],
        covulsion: json["covulsion"],
        hta: json["hta"],
        epoc: json["epoc"],
        acv: json["acv"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "historiaId": historiaId,
        "estadoId": estadoId,
        "diabetesI": diabetesI,
        "diabetesIi": diabetesIi,
        "asma": asma,
        "cardiopatia": cardiopatia,
        "covulsion": covulsion,
        "hta": hta,
        "epoc": epoc,
        "acv": acv,
    };
}
