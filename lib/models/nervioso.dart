import 'dart:convert';

Nervioso nerviosoFromJson(String str) => Nervioso.fromJson(json.decode(str));

String nerviosoToJson(Nervioso data) => json.encode(data.toJson());

class Nervioso {
    Nervioso({
        this.id,
        this.historiaId,
        this.estadoId,
        this.afasia,
        this.alteracion,
        this.alteracionSensibilidad,
        this.alteracionMotricidad,
        this.apraxia,
        this.coma,
        this.convulsionesFocales,
        this.convulsionesGenerales,
        this.corneal,
        this.deprimido,
        this.desorientado,
        this.desviacionConjugadaOjos,
        this.discordia,
        this.epistaxis,
        this.exitado,
        this.estuporoso,
        this.fotomotor,
        this.incontinenciaEsfinteres,
        this.lucido,
        this.miosis,
        this.midriasis,
        this.nistagmus,
        this.ojosMuneca,
        this.otorragia,
        this.otorraquia,
        this.otros,
        this.pupilaFija,
        this.rigidezNucaBabinsky,
        this.vigil,
    });

    int id;
    int historiaId;
    int estadoId;
    bool afasia;
    String alteracion;
    String alteracionSensibilidad;
    String alteracionMotricidad;
    bool apraxia;
    bool coma;
    bool convulsionesFocales;
    bool convulsionesGenerales;
    String corneal;
    bool deprimido;
    bool desorientado;
    bool desviacionConjugadaOjos;
    String discordia;
    bool epistaxis;
    bool exitado;
    bool estuporoso;
    String fotomotor;
    bool incontinenciaEsfinteres;
    bool lucido;
    String miosis;
    String midriasis;
    String nistagmus;
    bool ojosMuneca;
    bool otorragia;
    bool otorraquia;
    String otros;
    String pupilaFija;
    String rigidezNucaBabinsky;
    bool vigil;

    factory Nervioso.fromJson(Map<String, dynamic> json) => Nervioso(
        id: json["id"],
        historiaId: json["historiaId"],
        estadoId: json["estadoId"],
        afasia: json["afasia"],
        alteracion: json["alteracion"],
        alteracionSensibilidad: json["alteracionSensibilidad"],
        alteracionMotricidad: json["alteracionMotricidad"],
        apraxia: json["apraxia"],
        coma: json["coma"],
        convulsionesFocales: json["convulsionesFocales"],
        convulsionesGenerales: json["convulsionesGenerales"],
        corneal: json["corneal"],
        deprimido: json["deprimido"],
        desorientado: json["desorientado"],
        desviacionConjugadaOjos: json["desviacionConjugadaOjos"],
        discordia: json["discordia"],
        epistaxis: json["epistaxis"],
        exitado: json["exitado"],
        estuporoso: json["estuporoso"],
        fotomotor: json["fotomotor"],
        incontinenciaEsfinteres: json["incontinenciaEsfinteres"],
        lucido: json["lucido"],
        miosis: json["miosis"],
        midriasis: json["midriasis"],
        nistagmus: json["nistagmus"],
        ojosMuneca: json["ojosMuneca"],
        otorragia: json["otorragia"],
        otorraquia: json["otorraquia"],
        otros: json["otros"],
        pupilaFija: json["pupilaFija"],
        rigidezNucaBabinsky: json["rigidezNucaBabinsky"],
        vigil: json["vigil"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "historiaId": historiaId,
        "estadoId": estadoId,
        "afasia": afasia,
        "alteracion": alteracion,
        "alteracionSensibilidad": alteracionSensibilidad,
        "alteracionMotricidad": alteracionMotricidad,
        "apraxia": apraxia,
        "coma": coma,
        "convulsionesFocales": convulsionesFocales,
        "convulsionesGenerales": convulsionesGenerales,
        "corneal": corneal,
        "deprimido": deprimido,
        "desorientado": desorientado,
        "desviacionConjugadaOjos": desviacionConjugadaOjos,
        "discordia": discordia,
        "epistaxis": epistaxis,
        "exitado": exitado,
        "estuporoso": estuporoso,
        "fotomotor": fotomotor,
        "incontinenciaEsfinteres": incontinenciaEsfinteres,
        "lucido": lucido,
        "miosis": miosis,
        "midriasis": midriasis,
        "nistagmus": nistagmus,
        "ojosMuneca": ojosMuneca,
        "otorragia": otorragia,
        "otorraquia": otorraquia,
        "otros": otros,
        "pupila_fija": pupilaFija,
        "rigidezNucaBabinsky": rigidezNucaBabinsky,
        "vigil": vigil,
    };
}
