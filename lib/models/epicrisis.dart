import 'dart:convert';

Epicrisis epicrisisFromJson(String str) => Epicrisis.fromJson(json.decode(str));

String epicrisisToJson(Epicrisis data) => json.encode(data.toJson());

class Epicrisis {
    Epicrisis({
        this.id,
        this.historiaId,
        this.descripcion,
    });

    int id;
    int historiaId;
    String descripcion;

    factory Epicrisis.fromJson(Map<String, dynamic> json) => Epicrisis(
        id: json["id"],
        historiaId: json["historiaId"],
        descripcion: json["descripcion"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "historiaId": historiaId,
        "descripcion": descripcion,
    };
}
