import 'dart:convert';

Psiquiatrico psiquiatricoFromJson(String str) => Psiquiatrico.fromJson(json.decode(str));

String psiquiatricoToJson(Psiquiatrico data) => json.encode(data.toJson());

class Psiquiatrico {
    Psiquiatrico({
        this.id,
        this.historiaId,
        this.agresivo,
        this.alteracionConyugal,
        this.alteracionSocial,
        this.alteracionFamiliar,
        this.ansioso,
        this.cHisterica,
        this.depresivo,
        this.delirante,
        this.duelo,
        this.intentoSuicidio,
        this.otros,
        this.psicosisExogena,
        this.rva,
    });

    int id;
    int historiaId;
    bool agresivo;
    bool alteracionConyugal;
    bool alteracionSocial;
    bool alteracionFamiliar;
    bool ansioso;
    bool cHisterica;
    bool depresivo;
    bool delirante;
    bool duelo;
    bool intentoSuicidio;
    String otros;
    bool psicosisExogena;
    bool rva;

    factory Psiquiatrico.fromJson(Map<String, dynamic> json) => Psiquiatrico(
        id: json["id"],
        historiaId: json["historiaId"],
        agresivo: json["agresivo"],
        alteracionConyugal: json["alteracionConyugal"],
        alteracionSocial: json["alteracionSocial"],
        alteracionFamiliar: json["alteracionFamiliar"],
        ansioso: json["ansioso"],
        cHisterica: json["cHisterica"],
        depresivo: json["depresivo"],
        delirante: json["delirante"],
        duelo: json["duelo"],
        intentoSuicidio: json["intentoSuicidio"],
        otros: json["otros"],
        psicosisExogena: json["psicosisExogena"],
        rva: json["rva"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "historiaId": historiaId,
        "agresivo": agresivo,
        "alteracionConyugal": alteracionConyugal,
        "alteracionSocial": alteracionSocial,
        "alteracionFamiliar": alteracionFamiliar,
        "ansioso": ansioso,
        "cHisterica": cHisterica,
        "depresivo": depresivo,
        "delirante": delirante,
        "duelo": duelo,
        "intentoSuicidio": intentoSuicidio,
        "otros": otros,
        "psicosisExogena": psicosisExogena,
        "rva": rva,
    };
}