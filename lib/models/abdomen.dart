import 'dart:convert';

Abdomen abdomenFromJson(String str) => Abdomen.fromJson(json.decode(str));

String abdomenToJson(Abdomen data) => json.encode(data.toJson());

class Abdomen {
    Abdomen({
        this.id,
        this.historiaId,
        this.estadoId,
        this.ascitis,
        this.distendido,
        this.doloroso,
        this.enterorragia,
        this.esplenomegalia,
        this.eventracion,
        this.evisceracion,
        this.hematemesis,
        this.hepatomegalia,
        this.hernia,
        this.magBurney,
        this.melena,
        this.murphi,
        this.otros,
        this.peritonismo,
        this.ruidosHidroaereos,
        this.soplo,
        this.tactoRectal,
    });

    int id;
    int historiaId;
    int estadoId;
    bool ascitis;
    bool distendido;
    bool doloroso;
    bool enterorragia;
    bool esplenomegalia;
    bool eventracion;
    bool evisceracion;
    bool hematemesis;
    bool hepatomegalia;
    bool hernia;
    bool magBurney;
    bool melena;
    bool murphi;
    String otros;
    bool peritonismo;
    String ruidosHidroaereos;
    bool soplo;
    String tactoRectal;

    factory Abdomen.fromJson(Map<String, dynamic> json) => Abdomen(
        id: json["id"],
        historiaId: json["historiaId"],
        estadoId: json["estadoId"],
        ascitis: json["ascitis"],
        distendido: json["distendido"],
        doloroso: json["doloroso"],
        enterorragia: json["enterorragia"],
        esplenomegalia: json["esplenomegalia"],
        eventracion: json["eventracion"],
        evisceracion: json["evisceracion"],
        hematemesis: json["hematemesis"],
        hepatomegalia: json["hepatomegalia"],
        hernia: json["hernia"],
        magBurney: json["magBurney"],
        melena: json["melena"],
        murphi: json["murphi"],
        otros: json["otros"],
        peritonismo: json["peritonismo"],
        ruidosHidroaereos: json["ruidosHidroaereos"],
        soplo: json["soplo"],
        tactoRectal: json["tactoRectal"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "historiaId": historiaId,
        "estadoId": estadoId,
        "ascitis": ascitis,
        "distendido": distendido,
        "doloroso": doloroso,
        "enterorragia": enterorragia,
        "esplenomegalia": esplenomegalia,
        "eventracion": eventracion,
        "evisceracion": evisceracion,
        "hematemesis": hematemesis,
        "hepatomegalia": hepatomegalia,
        "hernia": hernia,
        "magBurney": magBurney,
        "melena": melena,
        "murphi": murphi,
        "otros": otros,
        "peritonismo": peritonismo,
        "ruidosHidroaereos": ruidosHidroaereos,
        "soplo": soplo,
        "tactoRectal": tactoRectal,
    };
}
