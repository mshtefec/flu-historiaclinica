import 'dart:convert';

Procedimiento procedimientoFromJson(String str) => Procedimiento.fromJson(json.decode(str));

String procedimientoToJson(Procedimiento data) => json.encode(data.toJson());

class Procedimiento {
    Procedimiento({
        this.id,
        this.historiaId,
        this.ambu,
        this.aspiracionTraqueal,
        this.arm,
        this.canulaMayo,
        this.cardioversion,
        this.chalecoExtricacion,
        this.collerEspinal,
        this.controlHemorragia,
        this.cricotirodoctomia,
        this.curacionHerida,
        this.desfibrilacion,
        this.drenajePleural,
        this.ferulaRigida,
        this.ferulaTraccion,
        this.ferulaInflable,
        this.ferulaVacio,
        this.intubacionTraqueal,
        this.lavajeEstomago,
        this.marcapasosExterno,
        this.marcapasosTransitorio,
        this.maniobraVagal,
        this.mascara02,
        this.monitoreo,
        this.nebulizacion,
        this.pantalonAntishock,
        this.rcp,
        this.sondajeVesical,
        this.suturaHerida,
        this.taplaEspinal,
        this.traqueostomia,
        this.vendaje,
        this.viaPeriferica,
        this.viaCentral,
        this.viaIntravenosa,
    });

    int id;
    int historiaId;
    bool ambu;
    bool aspiracionTraqueal;
    bool arm;
    bool canulaMayo;
    bool cardioversion;
    bool chalecoExtricacion;
    bool collerEspinal;
    bool controlHemorragia;
    bool cricotirodoctomia;
    bool curacionHerida;
    bool desfibrilacion;
    bool drenajePleural;
    bool ferulaRigida;
    bool ferulaTraccion;
    bool ferulaInflable;
    bool ferulaVacio;
    bool intubacionTraqueal;
    bool lavajeEstomago;
    bool marcapasosExterno;
    bool marcapasosTransitorio;
    bool maniobraVagal;
    bool mascara02;
    bool monitoreo;
    bool nebulizacion;
    bool pantalonAntishock;
    bool rcp;
    bool sondajeVesical;
    bool suturaHerida;
    bool taplaEspinal;
    bool traqueostomia;
    bool vendaje;
    bool viaPeriferica;
    bool viaCentral;
    bool viaIntravenosa;

    factory Procedimiento.fromJson(Map<String, dynamic> json) => Procedimiento(
        id: json["id"],
        historiaId: json["historiaId"],
        ambu: json["ambu"],
        aspiracionTraqueal: json["aspiracionTraqueal"],
        arm: json["arm"],
        canulaMayo: json["canulaMayo"],
        cardioversion: json["cardioversion"],
        chalecoExtricacion: json["chalecoExtricacion"],
        collerEspinal: json["collerEspinal"],
        controlHemorragia: json["controlHemorragia"],
        cricotirodoctomia: json["cricotirodoctomia"],
        curacionHerida: json["curacionHerida"],
        desfibrilacion: json["desfibrilacion"],
        drenajePleural: json["drenajePleural"],
        ferulaRigida: json["ferulaRigida"],
        ferulaTraccion: json["ferulaTraccion"],
        ferulaInflable: json["ferulaInflable"],
        ferulaVacio: json["ferulaVacio"],
        intubacionTraqueal: json["intubacionTraqueal"],
        lavajeEstomago: json["lavajeEstomago"],
        marcapasosExterno: json["marcapasosExterno"],
        marcapasosTransitorio: json["marcapasosTransitorio"],
        maniobraVagal: json["maniobraVagal"],
        mascara02: json["mascara02"],
        monitoreo: json["monitoreo"],
        nebulizacion: json["nebulizacion"],
        pantalonAntishock: json["pantalonAntishock"],
        rcp: json["rcp"],
        sondajeVesical: json["sondajeVesical"],
        suturaHerida: json["suturaHerida"],
        taplaEspinal: json["taplaEspinal"],
        traqueostomia: json["traqueostomia"],
        vendaje: json["vendaje"],
        viaPeriferica: json["viaPeriferica"],
        viaCentral: json["viaCentral"],
        viaIntravenosa: json["viaIntravenosa"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "historiaId": historiaId,
        "ambu": ambu,
        "aspiracionTraqueal": aspiracionTraqueal,
        "arm": arm,
        "canulaMayo": canulaMayo,
        "cardioversion": cardioversion,
        "chalecoExtricacion": chalecoExtricacion,
        "collerEspinal": collerEspinal,
        "controlHemorragia": controlHemorragia,
        "cricotirodoctomia": cricotirodoctomia,
        "curacionHerida": curacionHerida,
        "desfibrilacion": desfibrilacion,
        "drenajePleural": drenajePleural,
        "ferulaRigida": ferulaRigida,
        "ferulaTraccion": ferulaTraccion,
        "ferulaInflable": ferulaInflable,
        "ferulaVacio": ferulaVacio,
        "intubacionTraqueal": intubacionTraqueal,
        "lavajeEstomago": lavajeEstomago,
        "marcapasosExterno": marcapasosExterno,
        "marcapasosTransitorio": marcapasosTransitorio,
        "maniobraVagal": maniobraVagal,
        "mascara02": mascara02,
        "monitoreo": monitoreo,
        "nebulizacion": nebulizacion,
        "pantalonAntishock": pantalonAntishock,
        "rcp": rcp,
        "sondajeVesical": sondajeVesical,
        "suturaHerida": suturaHerida,
        "taplaEspinal": taplaEspinal,
        "traqueostomia": traqueostomia,
        "vendaje": vendaje,
        "viaPeriferica": viaPeriferica,
        "viaCentral": viaCentral,
        "viaIntravenosa": viaIntravenosa,
    };
}
