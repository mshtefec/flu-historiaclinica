import 'package:flutter/material.dart';

class InicioPage extends StatelessWidget {
  
  const InicioPage({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(80.0),
        child: AppBar(
            backgroundColor: Color.fromRGBO(53, 201, 201, 1),
            title: Padding(
                padding: EdgeInsets.only(top: 20),
                child: Text('PANEL DE USUARIO')),
            elevation: 1,
            actions: <Widget>[
                Padding(
                padding: EdgeInsets.only(top: 25,right: 15),
                child: Text('Cambiar contraseña', style: TextStyle(fontSize: 20.0))),
                Padding(
                padding: EdgeInsets.only(top: 25, right:20),
                child: Text('Salir', style: TextStyle(fontSize: 20.0)))
              ],
            ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          const SizedBox(height:20.0),
          Padding(
            padding: EdgeInsets.all(20.0),
            child: Image.asset('assets/fondo_panel.png',width: 400.0,)
            ),
          const SizedBox(height:20.0),
          Expanded(child: Text('En espera de asignación de servicio...', style: TextStyle(fontSize: 24.0, color:Color.fromRGBO(5, 5, 5, 0.5) ))),
          _footer()
        ],)
    );
  }
  
  _footer() {
    return Container(
          width: double.infinity,
          height:45.0,
          color: Color.fromRGBO(5, 5, 5, 0.8),
          child: Padding(
            padding: const EdgeInsets.only(top:5.0, left:16.0, right: 20.0),
            child: Row(
              children: <Widget>[
                Text('Dr. Avelino Castelán', style: TextStyle(fontSize: 14.0, color: Colors.white70),),
                Expanded(child:SizedBox(height:5)),
                Text('Conectado', style: TextStyle(fontSize: 14.0, color: Colors.white70),),
                const SizedBox(width: 5.0,),
                Padding(
                  padding: const EdgeInsets.only(top: 1.0),
                  child: CircleAvatar(backgroundColor: Color.fromRGBO(0, 200, 0, 0.8), radius: 5.0,),
                )
              ],
            ),
          ),
        );
  }


}
