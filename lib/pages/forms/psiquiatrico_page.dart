import 'package:flutter/material.dart';
import 'package:historia_clinica/widgets/campo_descriptivo.dart';
import 'package:historia_clinica/widgets/menu_lateral.dart';

class PsiquiatricoPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Historia Clinica'),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                MenuLateral(),
                Container(
                  margin: EdgeInsets.all(24.0),
                  width: 820.0,
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                           _campoCheck('Depresivo'),
                           _campoCheck('C. histérica'),
                           _campoCheck('Duelo'),
                           _campoCheck('Alteración social')
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                           _campoCheck('Ansioso'),
                           _campoCheck('RVA'),
                           _campoCheck('Psicosis exógena'),
                           _campoCheck('Alteración familiar')
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                           _campoCheck('Agresivo'),
                           _campoCheck('Delirante'),
                           _campoCheck('Intento de suicidio'),
                           _campoCheck('Alteración conyugal')
                        ],
                      ),
                      const SizedBox(height:20.0),
                      CampoDescriptivo(label: 'Otros'),
                      CampoDescriptivo(label: 'Cabeza - Cuello')
                    ],
                  ),
                ),
              ],
            ),
          ),
          _footer()
        ],
      ),
    );
  }

  _campoCheck(String label) {
    return Expanded(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Checkbox(value: null, onChanged: null),
          Text(label),
        ],
      ),
    );
  }

  _footer() {
    return Container(
      width: double.infinity,
      height: 45.0,
      color: Color.fromRGBO(5, 5, 5, 0.8),
      child: Padding(
        padding: const EdgeInsets.only(top: 5.0, left: 16.0, right: 20.0),
        child: Row(
          children: <Widget>[
            Text(
              'Dr. Avelino Castelán',
              style: TextStyle(fontSize: 14.0, color: Colors.white70),
            ),
            Expanded(child: SizedBox(height: 5)),
            Text(
              'Conectado',
              style: TextStyle(fontSize: 14.0, color: Colors.white70),
            ),
            const SizedBox(
              width: 5.0,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 1.0),
              child: CircleAvatar(
                backgroundColor: Color.fromRGBO(0, 200, 0, 0.8),
                radius: 5.0,
              ),
            )
          ],
        ),
      ),
    );
  }
}
