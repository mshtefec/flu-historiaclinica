import 'package:flutter/material.dart';
import 'package:historia_clinica/widgets/campo_descriptivo.dart';
import 'package:historia_clinica/widgets/menu_lateral.dart';

class CardiovascularPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Historia Clinica'),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                MenuLateral(),
                SingleChildScrollView(
                  child: Container(
                    margin: EdgeInsets.all(24.0),
                    width: 760.0,
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                                'I              II                                                   ')
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            _campoCheck('R3(+)'),
                            _campoCheck('Ingur yugular(+)'),
                            _campoCheckDoble('Ruidos hipofonéticos')
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            _campoCheck('R4(+)'),
                            _campoCheck('Galope'),
                            _campoCheckDoble('Desdoblados')
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            _campoCheck('Frote pericárdico'),
                          ],
                        ),
                        const SizedBox(
                          height: 20.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text('Soplo sistólico'),
                            _campoCheck('Foco pul'),
                            _campoCheck('Foco aór'),
                            _campoCheck('Foco mit'),
                            _campoCheck('Foco tri'),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text('Soplo diastólico'),
                            _campoCheck('Foco pul'),
                            _campoCheck('Foco aór'),
                            _campoCheck('Foco mit'),
                            _campoCheck('Foco tri'),
                          ],
                        ),
                        const SizedBox(height: 10.0,),
                        CampoDescriptivo(label: 'Arritmia'),
                        const SizedBox(height: 10.0,),
                        CampoDescriptivo(label: 'Pulsos periféricos'),
                        const SizedBox(height: 10.0,),
                        CampoDescriptivo(label: 'Otros'),
                        const SizedBox(height: 10.0,),
                        CampoDescriptivo(label: 'Electrocardiograma'),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          _footer()
        ],
      ),
    );
  }

  _campoCheckDoble(String label) {
    return Expanded(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Checkbox(value: false, onChanged: null),
          Checkbox(value: false, onChanged: null),
          Text(label),
        ],
      ),
    );
  }

  _campoCheck(String label) {
    return Expanded(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Checkbox(value: false, onChanged: null),
          Text(label),
        ],
      ),
    );
  }

  _footer() {
    return Container(
      width: double.infinity,
      height: 45.0,
      color: Color.fromRGBO(5, 5, 5, 0.8),
      child: Padding(
        padding: const EdgeInsets.only(top: 5.0, left: 16.0, right: 20.0),
        child: Row(
          children: <Widget>[
            Text(
              'Dr. Avelino Castelán',
              style: TextStyle(fontSize: 14.0, color: Colors.white70),
            ),
            Expanded(child: SizedBox(height: 5)),
            Text(
              'Conectado',
              style: TextStyle(fontSize: 14.0, color: Colors.white70),
            ),
            const SizedBox(
              width: 5.0,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 1.0),
              child: CircleAvatar(
                backgroundColor: Color.fromRGBO(0, 200, 0, 0.8),
                radius: 5.0,
              ),
            )
          ],
        ),
      ),
    );
  }
}
