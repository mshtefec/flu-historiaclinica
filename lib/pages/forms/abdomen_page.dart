import 'package:flutter/material.dart';
import 'package:historia_clinica/widgets/campo_descriptivo.dart';
import 'package:historia_clinica/widgets/menu_lateral.dart';

class AbdomenPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Historia Clinica'),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                MenuLateral(),
                SingleChildScrollView(
                  child: Container(
                    margin: EdgeInsets.all(24.0),
                    width: 760.0,
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                             _campoCheck('Hernia'),
                             _campoCheck('Distendido'),
                             _campoCheck('Ruidos hidroaéreos (+)')
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            _campoCheck('Eventración'),
                            _campoCheck('Doloroso'),
                            _campoCheck('Ruidos hidroaéreos (-)'),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            _campoCheck('Evisceración'),
                            _campoCheck('Mag. Burney(+)'),
                            _campoCheck('Soplo'),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            _campoCheck('Hematemesis'),
                            _campoCheck('Peritonismo'),
                            _campoCheck('Esplenomegalia'),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            _campoCheck('Melena'),
                            _campoCheck('Murphi (*)'),
                            _campoCheck('Ascitis'),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            _campoCheck('Enterorragia'),
                            _campoCheck('Hepatomegalia'),
                            SizedBox(width: 250)
                          ],
                        ),
                        const SizedBox(height:20.0),
                        CampoDescriptivo(label: 'Tacto rectal',),
                        const SizedBox(height:20.0),
                        CampoDescriptivo(label: 'Otros',)
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          _footer()
        ],
      ),
    );
  }

  _campoCheck(String label) {
    return Expanded(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Checkbox(value: false, onChanged: null),
          Text(label),
        ],
      ),
    );
  }

  _footer() {
    return Container(
      width: double.infinity,
      height: 45.0,
      color: Color.fromRGBO(5, 5, 5, 0.8),
      child: Padding(
        padding: const EdgeInsets.only(top: 5.0, left: 16.0, right: 20.0),
        child: Row(
          children: <Widget>[
            Text(
              'Dr. Avelino Castelán',
              style: TextStyle(fontSize: 14.0, color: Colors.white70),
            ),
            Expanded(child: SizedBox(height: 5)),
            Text(
              'Conectado',
              style: TextStyle(fontSize: 14.0, color: Colors.white70),
            ),
            const SizedBox(
              width: 5.0,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 1.0),
              child: CircleAvatar(
                backgroundColor: Color.fromRGBO(0, 200, 0, 0.8),
                radius: 5.0,
              ),
            )
          ],
        ),
      ),
    );
  }
}
