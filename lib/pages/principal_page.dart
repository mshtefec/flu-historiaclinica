import 'package:flutter/material.dart';
import 'package:historia_clinica/widgets/menu_lateral.dart';

class PrincipalPage extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Historia Clinica'),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                MenuLateral(),
                SingleChildScrollView(
                  child: Container(
                    margin: EdgeInsets.all(16.0),
                    width: 760.0,
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Container(
                              decoration: BoxDecoration(
                                  color: Theme.of(context).primaryColor,
                                  borderRadius: BorderRadius.circular(10.0)),
                              width: 700.0,
                              child: Padding(
                                padding: const EdgeInsets.all(12.0),
                                child: Text(
                                  'DATOS DEL PACIENTE',
                                  style: TextStyle(
                                      fontSize: 20.0, color: Colors.white),
                                ),
                              )),
                        ),
                        Text('Apellido y Nombre'),
                        TextField(),
                        Row(
                          children: [
                            Container(
                              width: 200.0,
                              child: Column(
                                children: [
                                  Text('Numero Afiliado'),
                                  TextField(),
                                ],
                              ),
                            ),
                            Container(
                              width: 200.0,
                              child: Column(
                                children: [
                                  Text('Edad'),
                                  TextField(),
                                ],
                              ),
                            ),
                            Expanded(
                              child: Column(
                                children: [
                                  Text('Genero'),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      Radio(
                                        groupValue: 1,
                                        onChanged: (int value) {},
                                        value: null,
                                      ),
                                      Radio(
                                        groupValue: 2,
                                        onChanged: (int value) {},
                                        value: null,
                                      ),
                                      Radio(
                                        groupValue: 3,
                                        onChanged: (int value) {},
                                        value: null,
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              width: 200.0,
                              child: Column(
                                children: [
                                  Text('Tipo Documento'),
                                  TextField(),
                                ],
                              ),
                            ),
                            Container(
                              width: 200.0,
                              child: Column(
                                children: [
                                  Text('Nro Documento'),
                                  TextField(),
                                ],
                              ),
                            ),
                            Container(
                              width: 200,
                              child: Column(
                                children: [
                                  Text('Médico de cabecera'),
                                  TextField(),
                                ],
                              ),
                            )
                          ],
                        ),
                        Container(
                            decoration: BoxDecoration(
                                color: Theme.of(context).primaryColor,
                                borderRadius: BorderRadius.circular(10.0)),
                            width: 700.0,
                            child: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Text(
                                'ANTECEDENTES',
                                style: TextStyle(
                                    fontSize: 20.0, color: Colors.white),
                              ),
                            )),
                        const SizedBox(height:14.0),
                        Row(
                          children: [
                            Expanded(
                                child: Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                                      children: [
                                        Checkbox(value: false, onChanged: null),
                                        Text('Diabetes I'),
                                        Checkbox(value: false, onChanged: null),
                                        Text('Diabetes I'),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                                      children: [
                                        Checkbox( value: false, onChanged: null),
                                        Text('Diabetes I'),
                                        Checkbox( value: false, onChanged: null),
                                        Text('Diabetes I'),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                                      children: [
                                        Checkbox(value: false, onChanged: null),
                                        Text('Diabetes I'),
                                        Checkbox(value: false, onChanged: null),
                                        Text('Diabetes I'),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                                      children: [
                                        Checkbox(value: false, onChanged: null),
                                        Text('Diabetes I'),
                                        Checkbox(value: false, onChanged: null),
                                        Text('Diabetes I'),
                                      ],
                                    ),
                                  ],
                                )
                              ),
                            Expanded(child: Center(child: Text('Medicamentos'))),
                          ],
                        ),
                        const SizedBox(height:20.0),
                        Row(
                          children: [
                            Expanded(
                              child: Column(
                                children: [
                                  Text('Alergia a:'),
                                  TextField(),
                                ],
                              ),
                            ),
                            Expanded(
                              child: Column(
                                children: [
                                  Text('Otros'),
                                  TextField(),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          _footer()
        ],
      ),
    );
  }

  _footer() {
    return Container(
      width: double.infinity,
      height: 45.0,
      color: Color.fromRGBO(5, 5, 5, 0.8),
      child: Padding(
        padding: const EdgeInsets.only(top: 5.0, left: 16.0, right: 20.0),
        child: Row(
          children: <Widget>[
            Text(
              'Dr. Avelino Castelán',
              style: TextStyle(fontSize: 14.0, color: Colors.white70),
            ),
            Expanded(child: SizedBox(height: 5)),
            Text(
              'Conectado',
              style: TextStyle(fontSize: 14.0, color: Colors.white70),
            ),
            const SizedBox(
              width: 5.0,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 1.0),
              child: CircleAvatar(
                backgroundColor: Color.fromRGBO(0, 200, 0, 0.8),
                radius: 5.0,
              ),
            )
          ],
        ),
      ),
    );
  }
}

