import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter/material.dart';
import 'package:historia_clinica/widgets/campo_descriptivo.dart';
import 'package:historia_clinica/widgets/menu_lateral.dart';

class DesenlacePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Historia Clinica'),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                MenuLateral(),
                SingleChildScrollView(
                  child: Container(
                    margin: EdgeInsets.all(24.0),
                    width: 760.0,
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            _campoCheck('Fallecido en domicilio'),
                            _campoCheck('Fallecido en traslado'),
                            _campoCheck('Queda en domicilio \ncon instrucciones'),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            _campoCheck('Ya retirado por otros medios'),
                            _campoCheck('Fallecido en nosocomio'),
                            _campoCheck('Falso llamado'),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            _campoCheck(
                                'Fallecido antes de la llegada del equipo de emergencia')
                          ],
                        ),
                        const SizedBox(height: 20.0),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text('Diagnostico Presuntivo'),
                            SimpleAutoCompleteTextField(
                                key: key,
                                decoration: new InputDecoration(
                                    errorText: "Seleccione una opción"),
                                controller: TextEditingController(text: ''),
                                suggestions: [],
                                textChanged: (text) => () {},
                                clearOnSubmit: true,
                                textSubmitted: (text) => () {}),
                          ],
                        ),
                        const SizedBox(height:20.0),
                        CampoDescriptivo(label: 'Epicrisis'),
                        const SizedBox(height:20.0),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Expanded(
                              flex: 4,
                              child: Column(
                                children: [
                                  Text('Tratamientos'),
                                  SimpleAutoCompleteTextField(
                                      key: key,
                                      decoration:
                                          new InputDecoration(errorText: ''),
                                      controller: TextEditingController(text: ''),
                                      suggestions: [],
                                      textChanged: (text) => () {},
                                      clearOnSubmit: true,
                                      textSubmitted: (text) => () {}),
                                ],
                              ),
                            ),
                            Expanded(
                              flex: 2,
                              child: 
                                  Column(
                                    children: [
                                      Text(''),
                                      SimpleAutoCompleteTextField(
                                          key: key,
                                          decoration:
                                              new InputDecoration(errorText: ''),
                                          controller: TextEditingController(text: ''),
                                          suggestions: [],
                                          textChanged: (text) => () {},
                                          clearOnSubmit: true,
                                          textSubmitted: (text) => () {}),
                                    ],
                                  ),
                              ),
                            Expanded(
                              flex: 1,
                              child: Column(children: [Text(''),TextField()])
                            ),
                            Expanded(
                              flex: 1,
                              child: IconButton( icon: Icon(Icons.add), onPressed: () {  },)
                              )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          _footer()
        ],
      ),
    );
  }

  _campoCheck(String label) {
    return Expanded(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Checkbox(value: false, onChanged: null),
          Text(label),
        ],
      ),
    );
  }

  _footer() {
    return Container(
      width: double.infinity,
      height: 45.0,
      color: Color.fromRGBO(5, 5, 5, 0.8),
      child: Padding(
        padding: const EdgeInsets.only(top: 5.0, left: 16.0, right: 20.0),
        child: Row(
          children: <Widget>[
            Text(
              'Dr. Avelino Castelán',
              style: TextStyle(fontSize: 14.0, color: Colors.white70),
            ),
            Expanded(child: SizedBox(height: 5)),
            Text(
              'Conectado',
              style: TextStyle(fontSize: 14.0, color: Colors.white70),
            ),
            const SizedBox(
              width: 5.0,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 1.0),
              child: CircleAvatar(
                backgroundColor: Color.fromRGBO(0, 200, 0, 0.8),
                radius: 5.0,
              ),
            )
          ],
        ),
      ),
    );
  }
}
