import 'package:flutter/material.dart';
import 'package:historia_clinica/widgets/boton_primary.dart';

class UbicacionPage extends StatelessWidget {
  
  const UbicacionPage({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(70.0),
        child: AppBar(
            backgroundColor: Color.fromRGBO(53, 201, 201, 1),
            title: Padding(
                padding: EdgeInsets.only(top: 20),
                child: Text('HISTORIA CLINICA - UBICACION DEL SERVICIO')),
            elevation: 1,
            ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          const SizedBox(height:40.0),
          Text('MAPA DE UBICACION', style: Theme.of(context).textTheme.headline6.copyWith(color: Colors.black54)),
          const SizedBox(height:20.0),
          Expanded(child: Text('')),
          BotonPrimary(texto: 'INICIAR SERVICIO', ancho: 180.0,),
          const SizedBox(height:20.0),
          _footer()
        ],)
    );
  }
  
  _footer() {
    return Container(
          width: double.infinity,
          height:45.0,
          color: Color.fromRGBO(5, 5, 5, 0.8),
          child: Padding(
            padding: const EdgeInsets.only(top:5.0, left:16.0, right: 20.0),
            child: Row(
              children: <Widget>[
                Text('Dr. Avelino Castelán', style: TextStyle(fontSize: 14.0, color: Colors.white70),),
                Expanded(child:SizedBox(height:5)),
                Text('Conectado', style: TextStyle(fontSize: 14.0, color: Colors.white70),),
                const SizedBox(width: 5.0,),
                Padding(
                  padding: const EdgeInsets.only(top: 1.0),
                  child: CircleAvatar(backgroundColor: Color.fromRGBO(0, 200, 0, 0.8), radius: 5.0,),
                )
              ],
            ),
          ),
        );
  }


}
