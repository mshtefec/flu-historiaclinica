import 'package:flutter/material.dart';
import 'package:historia_clinica/widgets/boton_primary.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(80.0),
        child: AppBar(
            backgroundColor: Color.fromRGBO(53, 201, 201, 1),
            title: Padding(
                padding: EdgeInsets.only(top: 20),
                child: Text('INICIO DE SESION')),
            elevation: 1),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(100.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                  width: double.infinity,
                  child: Center(
                      child: Text(
                    'LOGO',
                    style: TextStyle(fontSize: 60.0),
                  ))),
              const SizedBox(height: 120),
              _inputLogin(context, 'Usuario'),
              const SizedBox(height: 30),
              _inputLogin(context, 'Contraseña', esPassword: true),
              const SizedBox(height: 30),
              BotonPrimary(texto: 'INGRESAR',ancho: 200.0,),
              const SizedBox(height: 80.0),
              _reenvioContrasena()
            ],
          ),
        ),
      ),
    );
  }

  _reenvioContrasena() {
    return Text('Solicitar reenvío de contraseña',
        style: TextStyle(
            color: Color.fromRGBO(5, 5, 5, 0.7),
            fontSize: 24.0,
            decoration: TextDecoration.underline,
            decorationColor: Color.fromRGBO(5, 5, 5, 0.5)));
  }


  _inputLogin(BuildContext context, String texto, {bool esPassword = false}) {
    return Column(
      children: <Widget>[
        Align(
            alignment: Alignment.centerLeft,
            child: Text(
              texto,
              style: Theme.of(context).textTheme.headline5,
            )),
        TextField(
          obscureText: esPassword,
          decoration: InputDecoration(
              contentPadding: EdgeInsets.all(24.0),
              border: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.black),
                borderRadius: BorderRadius.circular(10.0),
              )),
        ),
      ],
    );
  }
}
