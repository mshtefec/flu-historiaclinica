import 'package:flutter/material.dart';
import 'file:///C:/Users/Agustin/AndroidStudioProjects/historia_clinica/lib/pages/forms/abdomen_page.dart';
import 'package:historia_clinica/pages/forms/cardiovascular_page.dart';
import 'package:historia_clinica/pages/desenlace_page.dart';
import 'file:///C:/Users/Agustin/AndroidStudioProjects/historia_clinica/lib/pages/forms/ginecobstetrico_page.dart';
import 'package:historia_clinica/pages/inicio_page.dart';
import 'package:historia_clinica/pages/login_page.dart';
import 'package:historia_clinica/pages/forms/nervioso_page.dart';
import 'package:historia_clinica/pages/parametros_basicos_page.dart';
import 'package:historia_clinica/pages/principal_page.dart';
import 'file:///C:/Users/Agustin/AndroidStudioProjects/historia_clinica/lib/pages/forms/procedimiento_page.dart';
import 'package:historia_clinica/pages/forms/psiquiatrico_page.dart';
import 'file:///C:/Users/Agustin/AndroidStudioProjects/historia_clinica/lib/pages/forms/respiratorio_page.dart';
import 'package:historia_clinica/pages/ubicacion_page.dart';
import 'package:historia_clinica/pages/forms/urogenital_page.dart';
import 'package:historia_clinica/pages/firma_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData.light()
          .copyWith(primaryColor: Color.fromRGBO(53, 201, 201, 1)),
      title: 'Historia Clinica EME',
      initialRoute: 'firma',
      routes: {
        'login' : (_) => LoginPage(),
        'inicio': (_) => InicioPage(),
        'ubicacion': (_) => UbicacionPage(),
        'principal': (_) => PrincipalPage(),
        'parametros': (_) => ParametrosBasicosPage(),
        'ginecobstetrico': (_) => GinecobstetricoPage(),
        'urogenital': (_) => UrogenitalPage(),
        'procedimiento': (_) => ProcedimientoPage(),
        'abdomen': (_) => AbdomenPage(),
        'nervioso': (_) => NerviosoPage(),
        'respiratorio': (_) => RespiratorioPage(),
        'cardiovascular': (_) => CardiovascularPage(),
        'psiquiatrico': (_) => PsiquiatricoPage(),
        'desenlace': (_) => DesenlacePage(),
        'firma': (_) => FirmaPage(),
      },
    );
  }
}

