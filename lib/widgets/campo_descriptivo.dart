import 'package:flutter/material.dart';

class CampoDescriptivo extends StatelessWidget {
  
  final String label;
  
  const CampoDescriptivo({
    Key key, this.label,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(label),
        TextField(
        ),
      ],
    );
  }
}