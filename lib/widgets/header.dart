import 'package:flutter/material.dart';

class Header extends StatelessWidget {
  
  final String texto;
  
  const Header({
    Key key, this.texto,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Container(
          decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
              borderRadius: BorderRadius.circular(10.0)),
          width: 700.0,
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Text(
              texto,
              style: TextStyle(
                  fontSize: 20.0, color: Colors.white),
            ),
          )),
    );
  }
}