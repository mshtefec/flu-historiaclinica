import 'package:flutter/material.dart';

 class BotonPrimary extends StatelessWidget {
 
   final String texto;
   final double ancho;

  const BotonPrimary({Key key, this.texto, this.ancho}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Container(
        height: 60.0,
        width: ancho,
        child: RaisedButton(
            elevation: 0.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            color: Color.fromRGBO(53, 201, 201, 1),
            child: Text(texto,
                style: TextStyle(
                    color: Color.fromRGBO(245, 245, 245, 1), fontSize: 16.0)),
            onPressed: () {}),
      ),
    );
  }   
}