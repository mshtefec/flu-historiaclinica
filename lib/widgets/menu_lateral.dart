import 'package:flutter/material.dart';

class MenuLateral extends StatelessWidget {
  
    final TextStyle estiloMenu = TextStyle(fontSize: 16.0, color: Colors.white70);
    final TextStyle estiloMenuActivo = TextStyle(fontSize: 16.0, color: Colors.white);

 MenuLateral({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 150,
      child: Flexible(
        child: Container(
          color: Theme.of(context).primaryColor,
          child: ListView(
            children: <Widget>[
              Container(
                height: 80.0,
                color: Colors.black,
              ),
              Divider(),
              ListTile(title: Text("Datos Paciente", style: estiloMenuActivo)),
              ListTile(title: Text("Parámetros Básicos", style: estiloMenu)),
              ListTile(title: Text("Formularios", style: estiloMenu)),
              ListTile(title: Text("Desenlace", style: estiloMenu)),
              ListTile(title: Text("Firma", style: estiloMenu)),
            ],
          ),
        ),
      ),
    );
  }
}
